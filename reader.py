import csv
import sys
import os

arguments = sys.argv[1:]
src = arguments[0]
dst = arguments[1]
changes = arguments[2:]

# Zrzucenie sciezki projektu do zmiennej our_path
our_path = os.getcwd()

# Sprawdzenie czy plik o sciezce src istnieje
if os.path.isfile(src):
    print(f"Znaleziono plik csv o ścieżce {src}")
else:
    print(f"Nie znalezniono pliku o sciezce {src}")
    files = os.listdir(our_path)
    print(f"Dostepne pliki z katalogu projektu to {files}")


read_rows = []
with open(src) as src_file:
    lines = csv.reader(src_file)
    for row in lines:
        read_rows.append(row)


print(read_rows)

# Transformacja listy zmian z argumentow z postaci ['X,Y,wartosc', 'X,Y,wartosc2] na [[X, Y, wartosc], [X, Y, wartosc2]]
lista_zmian = []
for i in range(len(changes)):
    wartosc_zmieniona_jako_lista = changes[i].split(',')
    lista_zmian.append(wartosc_zmieniona_jako_lista)

for i in range(len(lista_zmian)): # -> [X, Y, wartosc] - lista_zmian[i] -> lista_zmian[i][0] -> X
    for j in range(len(read_rows)):
        if j == int(lista_zmian[i][0]):
            read_rows[i][int(lista_zmian[i][1])] = lista_zmian[i][2]


with open(dst, 'w') as dst_file:
    writer = csv.writer(dst_file)
    for row in read_rows:
        writer.writerow(row)


